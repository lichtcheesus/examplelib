import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="exampleLib",
    version="1.0",
    author="Max Mustermann",
    description="Example library",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Development Status :: 1 - Pre-Alpha"
    ],
    install_requires=[
    'requests==2.21.0']
)
