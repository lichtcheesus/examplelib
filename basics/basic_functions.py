# -*- coding: utf-8 -*-
import logging
import os


logging.basicConfig(format='%(asctime)s - [%(processName)s - %(threadName)s] %(name)s - %(levelname)s - %(message)s',
                    datefmt="%Y-%m-%d %H:%M:%S", level=os.getenv('LOGLEVEL', 'INFO'))

logger = logging.getLogger(__name__)


def example_basic_function():
    logger.info('This is a basic function.')